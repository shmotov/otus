### MongoDB  
[Manual MongoDB](https://docs.mongodb.com/manual)  
Цель: В результате выполнения ДЗ вы настроите реплицирование и шардирование, аутентификацию в кластере и проверите отказоустойчивость.  
Необходимо:
- построить шардированный кластер из 3 кластерных нод( по 3 инстанса с репликацией) и с кластером конфига(3 инстанса);
- добавить балансировку, нагрузить данными, выбрать хороший ключ шардирования, посмотреть как данные перебалансируются между шардами;
- настроить аутентификацию и многоролевой доступ;
- поронять разные инстансы, посмотреть, что будет происходить, поднять обратно. Описать что произошло.
---
_В целях обучения все инстансы будем разворачивать на одной уже существующей ноде с установленной MongoDB 4.4.1_  
1. Создаем каталоги под БД, даем полный доступ (3 шарда, 3 репликсета под каждый шард):
```
$ sudo mkdir /home/mongo
$ sudo mkdir /home/mongo/{sdb1N1,sdb1N2,sdb1N3,sdb2N1,sdb2N2,sdb2N3,sdb3N1,sdb3N2,sdb3N3}  
$ sudo chmod 777 /home/mongo/{sdb1N1,sdb1N2,sdb1N3,sdb2N1,sdb2N2,sdb2N3,sdb3N1,sdb3N2,sdb3N3}
```
2. Запускаем процессы (параметр --shardsvr указывает, что БД будет частью шардированной БД):
```
$ mongod --shardsvr --dbpath /home/mongo/sdb1N1 --port 27011 --replSet RS1 --fork --logpath /home/mongo/sdb1N1/sdb1N1.log --pidfilepath /home/mongo/sdb1N1/sdb1N1.pid  
$ mongod --shardsvr --dbpath /home/mongo/sdb1N2 --port 27012 --replSet RS1 --fork --logpath /home/mongo/sdb1N2/sdb1N2.log --pidfilepath /home/mongo/sdb1N2/sdb1N2.pid  
$ mongod --shardsvr --dbpath /home/mongo/sdb1N3 --port 27013 --replSet RS1 --fork --logpath /home/mongo/sdb1N3/sdb1N3.log --pidfilepath /home/mongo/sdb1N3/sdb1N3.pid  
  
$ mongod --shardsvr --dbpath /home/mongo/sdb2N1 --port 27021 --replSet RS2 --fork --logpath /home/mongo/sdb2N1/sdb2N1.log --pidfilepath /home/mongo/sdb2N1/sdb2N1.pid  
$ mongod --shardsvr --dbpath /home/mongo/sdb2N2 --port 27022 --replSet RS2 --fork --logpath /home/mongo/sdb2N2/sdb2N2.log --pidfilepath /home/mongo/sdb2N2/sdb2N2.pid  
$ mongod --shardsvr --dbpath /home/mongo/sdb2N3 --port 27023 --replSet RS2 --fork --logpath /home/mongo/sdb2N3/sdb2N3.log --pidfilepath /home/mongo/sdb2N3/sdb2N3.pid  
  
$ mongod --shardsvr --dbpath /home/mongo/sdb3N1 --port 27031 --replSet RS3 --fork --logpath /home/mongo/sdb3N1/sdb3N1.log --pidfilepath /home/mongo/sdb3N1/sdb3N1.pid  
$ mongod --shardsvr --dbpath /home/mongo/sdb3N2 --port 27032 --replSet RS3 --fork --logpath /home/mongo/sdb3N2/sdb3N2.log --pidfilepath /home/mongo/sdb3N2/sdb3N2.pid  
$ mongod --shardsvr --dbpath /home/mongo/sdb3N3 --port 27033 --replSet RS3 --fork --logpath /home/mongo/sdb3N3/sdb3N3.log --pidfilepath /home/mongo/sdb3N3/sdb3N3.pid 
```
3. Проверим, что процессы запущены:
```
$ ps aux | grep mongo| grep -Ev "grep"  
```
4. Инициализируем все репликсеты (в каждом из репликсетов одну из БД назначаем арбитром, для примера), проверяем создание
```
$ mongo --port 27011  
rs.initiate({"_id" : "RS1", members : [{"_id" : 0, priority : 3, host : "127.0.0.1:27011"},{"_id" : 1, host : "127.0.0.1:27012"},{"_id" : 2, host : "127.0.0.1:27013", arbiterOnly : true}]})  
rs.status()  
exit  
  
$ mongo --port 27021  
rs.initiate({"_id" : "RS2", members : [{"_id" : 0, priority : 3, host : "127.0.0.1:27021"},{"_id" : 1, host : "127.0.0.1:27022"},{"_id" : 2, host : "127.0.0.1:27023", arbiterOnly : true}]})  
rs.status()  
exit  
  
$ mongo --port 27031  
rs.initiate({"_id" : "RS3", members : [{"_id" : 0, priority : 3, host : "127.0.0.1:27031"},{"_id" : 1, host : "127.0.0.1:27032"},{"_id" : 2, host : "127.0.0.1:27033", arbiterOnly : true}]})  
rs.status()  
exit  
```
5. Создаем в репликсетах пользователей с разными правами:
```
$ mongo --port 27011
use admin  
db.createUser({  
     user: "myroot",  
     pwd: "myroot",  
     roles: [{ role: "root", db: "admin" }]  
}) 
db.createUser({  
     user: "user",  
     pwd: "user",  
     roles: [{ role: "readWrite", db: "myDB" }]  
})
exit  
  
$ mongo --port 27021
use admin  
db.createUser({  
     user: "myroot",  
     pwd: "myroot",  
     roles: [{ role: "root", db: "admin" }]  
}) 
db.createUser({  
     user: "user",  
     pwd: "user",  
     roles: [{ role: "readWrite", db: "myDB" }]  
})
exit  
  
$ mongo --port 27031
use admin  
db.createUser({  
     user: "myroot",  
     pwd: "myroot",  
     roles: [{ role: "root", db: "admin" }]  
}) 
db.createUser({  
     user: "user",  
     pwd: "user",  
     roles: [{ role: "readWrite", db: "myDB" }]  
})
exit 
```
6. Создаем каталоги под конфигурацию, даем полный доступ (кластер конфигурации будет из трех инстансов, плюс отдельно под шард):
```
$ sudo mkdir /home/mongo/{cfgN1,cfgN2,cfgN3,sh}  
$ sudo chmod 777 /home/mongo/{cfgN1,cfgN2,cfgN3,sh}
```
7. Запускаем процессы конфигурации (параметр --configsvr указывает, что БД будет для хранения конфигурации):
```
$ mongod --configsvr --dbpath /home/mongo/cfgN1 --port 27001 --replSet RScfg --fork --logpath /home/mongo/cfgN1/cfgN1.log --pidfilepath /home/mongo/cfgN1/cfgN1.pid  
$ mongod --configsvr --dbpath /home/mongo/cfgN2 --port 27002 --replSet RScfg --fork --logpath /home/mongo/cfgN2/cfgN2.log --pidfilepath /home/mongo/cfgN2/cfgN2.pid  
$ mongod --configsvr --dbpath /home/mongo/cfgN3 --port 27003 --replSet RScfg --fork --logpath /home/mongo/cfgN3/cfgN3.log --pidfilepath /home/mongo/cfgN3/cfgN3.pid  
```
8. Инициализируем репликсет конфигурации (обязательно без арбитра):
```
$ mongo --port 27001  
rs.initiate({"_id" : "RScfg", members : [{"_id" : 0, priority : 3, host : "127.0.0.1:27001"},{"_id" : 1, host : "127.0.0.1:27002"},{"_id" : 2, host : "127.0.0.1:27003"}]})  
```
9. Создаем в репликсете конфигурации пользователей с разными правами:
```
use admin  
db.createUser({  
     user: "myroot",  
     pwd: "myroot",  
     roles: [{ role: "root", db: "admin" }]  
}) 
db.createUser({  
     user: "user",  
     pwd: "user",  
     roles: [{ role: "readWrite", db: "myDB" }]  
})
exit  
```
10. Для авторизации создаем ключ-файл:
```
$ cd /home/mongo/
$ openssl rand -base64 756 > /home/mongo/keyfile  
$ chmod 400 keyfile  
```
11. Запускаем все текущие процессы с ключами --auth и --keyfile для включения аутентификации (предварительно убив их с помощью kill):
```
$ ps aux | grep mongo| grep -Ev "grep"  
$ kill -9 ....
  
$ mongod --shardsvr --dbpath /home/mongo/sdb1N1 --port 27011 --replSet RS1 --auth --fork --logpath /home/mongo/sdb1N1/sdb1N1.log --pidfilepath /home/mongo/sdb1N1/sdb1N1.pid --keyFile /home/mongo/keyfile  
$ mongod --shardsvr --dbpath /home/mongo/sdb1N2 --port 27012 --replSet RS1 --auth --fork --logpath /home/mongo/sdb1N2/sdb1N2.log --pidfilepath /home/mongo/sdb1N2/sdb1N2.pid --keyFile /home/mongo/keyfile  
$ mongod --shardsvr --dbpath /home/mongo/sdb1N3 --port 27013 --replSet RS1 --auth --fork --logpath /home/mongo/sdb1N3/sdb1N3.log --pidfilepath /home/mongo/sdb1N3/sdb1N3.pid --keyFile /home/mongo/keyfile  
  
$ mongod --shardsvr --dbpath /home/mongo/sdb2N1 --port 27021 --replSet RS2 --auth --fork --logpath /home/mongo/sdb2N1/sdb2N1.log --pidfilepath /home/mongo/sdb2N1/sdb2N1.pid --keyFile /home/mongo/keyfile  
$ mongod --shardsvr --dbpath /home/mongo/sdb2N2 --port 27022 --replSet RS2 --auth --fork --logpath /home/mongo/sdb2N2/sdb2N2.log --pidfilepath /home/mongo/sdb2N2/sdb2N2.pid --keyFile /home/mongo/keyfile  
$ mongod --shardsvr --dbpath /home/mongo/sdb2N3 --port 27023 --replSet RS2 --auth --fork --logpath /home/mongo/sdb2N3/sdb2N3.log --pidfilepath /home/mongo/sdb2N3/sdb2N3.pid --keyFile /home/mongo/keyfile  
  
$ mongod --shardsvr --dbpath /home/mongo/sdb3N1 --port 27031 --replSet RS3 --auth --fork --logpath /home/mongo/sdb3N1/sdb3N1.log --pidfilepath /home/mongo/sdb3N1/sdb3N1.pid --keyFile /home/mongo/keyfile  
$ mongod --shardsvr --dbpath /home/mongo/sdb3N2 --port 27032 --replSet RS3 --auth --fork --logpath /home/mongo/sdb3N2/sdb3N2.log --pidfilepath /home/mongo/sdb3N2/sdb3N2.pid --keyFile /home/mongo/keyfile  
$ mongod --shardsvr --dbpath /home/mongo/sdb3N3 --port 27033 --replSet RS3 --auth --fork --logpath /home/mongo/sdb3N3/sdb3N3.log --pidfilepath /home/mongo/sdb3N3/sdb3N3.pid --keyFile /home/mongo/keyfile  
  
$ mongod --configsvr --dbpath /home/mongo/cfgN1 --port 27001 --replSet RScfg --auth --fork --logpath /home/mongo/cfgN1/cfgN1.log --pidfilepath /home/mongo/cfgN1/cfgN1.pid --keyFile /home/mongo/keyfile  
$ mongod --configsvr --dbpath /home/mongo/cfgN2 --port 27002 --replSet RScfg --auth --fork --logpath /home/mongo/cfgN2/cfgN2.log --pidfilepath /home/mongo/cfgN2/cfgN2.pid --keyFile /home/mongo/keyfile  
$ mongod --configsvr --dbpath /home/mongo/cfgN3 --port 27003 --replSet RScfg --auth --fork --logpath /home/mongo/cfgN3/cfgN3.log --pidfilepath /home/mongo/cfgN3/cfgN3.pid --keyFile /home/mongo/keyfile
```
12. Проверяем аутентификацию:
```
$ mongo --port 27011  
rs.status()  
```
> "errmsg" : "command replSetGetStatus requires authentication"  
```
exit  
$ mongo --port 27011 -u "myroot" -p "myroot"  
rs.status()  
```
> "ok" : 1  
12. Создаем шардированный кластер (и запустим его демона в двух экземплярах для отказоустойчивости):
```
exit  
$ mongos --configdb RScfg/127.0.0.1:27001,127.0.0.1:27002,127.0.0.1:27003 --port 27000 --fork --logpath /home/mongo/sh/sh.log --keyFile /home/mongo/keyfile  
$ mongos --configdb RScfg/127.0.0.1:27001,127.0.0.1:27002,127.0.0.1:27003 --port 27100 --fork --logpath /home/mongo/sh/sh2.log --keyFile /home/mongo/keyfile   
```
13. Включаем в шардированный кластер подготовленные кластеры БД:
```
$ mongo --port 27000 -u "myroot" -p "myroot"  
sh.addShard("RS1/127.0.0.1:27011,127.0.0.1:27012,127.0.0.1:27013")  
sh.addShard("RS2/127.0.0.1:27021,127.0.0.1:27022,127.0.0.1:27023")  
sh.addShard("RS3/127.0.0.1:27031,127.0.0.1:27032,127.0.0.1:27033")
```
14. Проверим, все ли ок, выведем данные по шарду:
```
sh.status()  
```
> --- Sharding Status ---  
> sharding version: {  
>        "_id" : 1,  
>        "minCompatibleVersion" : 5,  
>        "currentVersion" : 6,  
>        "clusterId" : ObjectId("5f9579ec5428c2b6e47e0e9d")  
>  }  
>  shards:  
>        {  "_id" : "RS1",  "host" : "RS1/127.0.0.1:27011,127.0.0.1:27012",  "state" : 1 }  
>        {  "_id" : "RS2",  "host" : "RS2/127.0.0.1:27021,127.0.0.1:27022",  "state" : 1 }  
>        {  "_id" : "RS3",  "host" : "RS3/127.0.0.1:27031,127.0.0.1:27032",  "state" : 1 }  
> ...  
15. Проверим, включена ли балансировка шардирования:
```
sh.getBalancerState() 
```
> true  
16. Укажем размер чанка для примера 1МБ (по умолчанию 64МБ):
```
use config  
db.settings.save({ _id:"chunksize", value: 1})
```
17. Создадим БД myDB и включим для нее шардирование:
```
use myDB  
sh.enableSharding("myDB")
```
18. Заполним коллекцию temp рандомными данными:
```
for (var i=1; i<200000; i++){  
    db.tempr.insert({day: i, name: "max. temperature of day "+i, amount: Math.random()*40})  
}
db.tempr.find().count()
```
19. Проиндексируем коллекцию по _id и шардируем коллекцию по нему:
```
db.tempr.createIndex({day: 1})  
sh.shardCollection("myDB.tempr",{day: 1 })
```
20. Проверим, разложились ли данные по шардам (через некоторое время):
```
db.tempr.getShardDistribution()
```
> Totals  
> data : 17.44MiB docs : 199999 chunks : 35  
> Shard RS1 contains 31.58% data, 31.69% docs in cluster, avg obj size on shard : 91B  
> Shard RS3 contains 34.47% data, 34.57% docs in cluster, avg obj size on shard : 91B  
> Shard RS2 contains 33.94% data, 33.73% docs in cluster, avg obj size on shard : 92B  
21. Проверим, работает ли аутентификация в mongos:
```
exit  
$ mongo --port 27000 -u "user" -p "user" 
sh.status()  
```
> uncaught exception: Error: error: {  
>         "ok" : 0,  
>         "errmsg" : "not authorized on config to execute command..."  
22. Для эксперимента убиваем один инстанс реплики (primary RS1 с PID=3027):
```
exit  
$ ps aux | grep mongo| grep -Ev "grep"  
$ kill -9 3027  
```
23. И смотрим, что происходит (инстанс на порту 27012 стал primary, 27011 потерялся, но все работает дальше):  
```
$ mongo --port 27012 -u "myroot" -p "myroot"   
rs.status()  
```
> RS1:PRIMARY> rs.status()  
> ...  
> "name" : "127.0.0.1:27011",  
> "health" : 0,  
> "state" : 8,  
> "stateStr" : "(not reachable/healthy)"  
```
exit  
$ mongo --port 27000 -u "myroot" -p "myroot"   
sh.status()  
```
> --- Sharding Status ---  
>  ...  
>  shards:  
>        {  "_id" : "RS1",  "host" : "RS1/127.0.0.1:27011,127.0.0.1:27012",  "state" : 1 }  
>        {  "_id" : "RS2",  "host" : "RS2/127.0.0.1:27021,127.0.0.1:27022",  "state" : 1 }  
>        {  "_id" : "RS3",  "host" : "RS3/127.0.0.1:27031,127.0.0.1:27032",  "state" : 1 }  
24. Запускаем инстанс (все ок, инстанс на порту 27011 сразу становится primary):  
```
exit  
$ mongod --shardsvr --dbpath /home/mongo/sdb1N1 --port 27011 --replSet RS1 --fork --logpath /home/mongo/sdb1N1/sdb1N1.log --keyFile /home/mongo/keyfile  
$ mongo --port 27011 -u "myroot" -p "myroot"   
rs.status()  
```
> RS1:PRIMARY> rs.status()  
> ...  
> "name" : "127.0.0.1:27011",  
> "health" : 1,  
> "state" : 1,  
> "stateStr" : "PRIMARY"  
