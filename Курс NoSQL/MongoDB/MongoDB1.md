### MongoDB  
[Manual MongoDB](https://docs.mongodb.com/manual)  
Цель: В результате выполнения ДЗ вы научитесь разворачивать MongoDB, заполнять данными и делать запросы.  
Необходимо:
- установить MongoDB одним из способов: ВМ, докер;
- заполнить данными;
- написать несколько запросов на выборку и обновление данных.
---

1. Создаём вручную в Console GCP виртуальную машину mongo. Также можно использовать команду:
```
$ gcloud beta compute --project=optical-loop-291504 instances create mongo --zone=us-central1-a --machine-type=e2-medium --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=838710683589-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --image=ubuntu-2004-focal-v20200907 --image-project=ubuntu-os-cloud --boot-disk-size=10GB --boot-disk-type=pd-ssd --boot-disk-device-name=mongo --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any
```
2. Открываем SSH-терминал для работы с ВМ (**обязательно в Google Chrome!**).
3. Устанавливаем mongoDB: 
```
$ wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add - && echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list && sudo apt-get update && sudo apt-get install -y mongodb-org
```
4. Создаем каталог БД и даем полный доступ до него: 
```
$ sudo mkdir /home/mongo &&  sudo mkdir /home/mongo/dbtest && sudo chmod 777 /home/mongo/dbtest
```
5. Запускаем процесс:
```
$ mongod --dbpath /home/mongo/dbtest --port 27001 --fork --logpath /home/mongo/dbtest/dbtest.log --pidfilepath /home/mongo/dbtest/dbtest.pid
```
6. Подключаемся к БД: 
```
$ mongo --port 27001
```
> MongoDB shell version v4.4.1  
> connecting to: mongodb://127.0.0.1:27001/?compressors=disabled&gssapiServiceName=mongodb
7. Создаем новую базу myDB:
```
use myDB
```
8. Вставляем записи (коллекция создается автоматически)
```
db.users.insert([  
{ user: { login: "mng1", name: "Nick"}, id_group: 1, role: [ "manager" ] },  
{ user: { login: "mng2", name: "Bob"}, id_group: 2, role: [ "manager", "security" ] },  
{ user: { login: "boss1", name: "Alice"}, id_group: 3, role: [ "boss"] },  
{ user: { login: "mng3", name: "Helen"}, id_group: 2, role: [ "manager", "security" ] },  
])
```
```
db.users.insert([  
{ username: "Mike", source: 2, role: [ "manager" ] },  
{ username: "Alice", source: 2, role: [ "manager" ] }, 
])
```
9. Обновим запись
```
db.runCommand( { update: "users", updates: [{ q: { role: "boss" }, u: { $set: { id_group: 666 }} }], ordered: false } )
```
10. Делаем запросы данных

- Найдем всех менеджеров
```
db.users.find({"role" : "manager"})
```
> { "_id" : ObjectId("5f859b8f4d0189e4fdc27c3f"), "user" : { "login" : "mng1", "name" : "Nick" }, "id_group" : 1, "role" : [ "manager" ] }  
> { "_id" : ObjectId("5f859b8f4d0189e4fdc27c40"), "user" : { "login" : "mng2", "name" : "Bob" }, "id_group" : 2, "role" : [ "manager", "security" ] }  
> { "_id" : ObjectId("5f859b8f4d0189e4fdc27c42"), "user" : { "login" : "mng3", "name" : "Helen" }, "id_group" : 2, "role" : [ "manager", "security" ] }  
> { "_id" : ObjectId("5f859c344d0189e4fdc27c43"), "username" : "Mike", "source" : 2, "role" : [ "manager" ] }  
> { "_id" : ObjectId("5f859c344d0189e4fdc27c44"), "username" : "Alice", "source" : 2, "role" : [ "manager" ] }  
- Найдем всех Alice
```
db.users.find({ $or: [{"user.name":"Alice"},{"username":"Alice"}] })
```
> { "_id" : ObjectId("5f85a04a1a965e8adb7ae4ad"), "user" : { "login" : "boss1", "name" : "Alice" }, "id_group" : 666, "role" : [ "boss" ] }  
> { "_id" : ObjectId("5f85a0541a965e8adb7ae4b0"), "username" : "Alice", "source" : 2, "role" : [ "manager" ] }  
11. Очищаем коллекцию от данных
```
db.runCommand( { delete: "users", deletes: [ { q: { }, limit: 0 }] } )
```
12. Удаляем коллекцию
```
db.users.drop()
```
13. Удаляем БД
```
db.dropDatabase()
```